#ifndef BUFFERPARASEINTERFACE_H
#define BUFFERPARASEINTERFACE_H

#include <iostream>
#include <vector>
#include "INS570D/INS570D_Define.h"
#include "INS_STATUS.h"

class BufferParaseInterface
{
public:
    BufferParaseInterface();

    ~BufferParaseInterface();

    virtual bool setBuffer(const std::vector<uint8_t> &buf_){return false;}

    virtual bool setBuffer(const std::string &buf_){}

    virtual bool setBuffer(const std::vector<uint8_t>&buf, int start_, int end_) { return false; }

    virtual void parse()=0;

    virtual INS_STATUS& getInsData()=0;

    virtual RTK_STATUS& getRTKData()=0;

    virtual INS_STATUS& getIMUData()=0;

    virtual uint8_t checksum()=0;

    //virtual void toRosMsg();
};

#endif // BUFFERPARASEINTERFACE_H
