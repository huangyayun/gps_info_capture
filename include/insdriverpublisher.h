#ifndef INSDRIVERPUBLISHER_H
#define INSDRIVERPUBLISHER_H
#include <iostream>
#include <ros/ros.h>
#include <std_msgs/String.h>
#include <sensor_msgs/NavSatFix.h>
#include <sensor_msgs/Imu.h>
#include <nav_msgs/Odometry.h>
#include <geometry_msgs/PoseStamped.h>
#include <geometry_msgs/TwistStamped.h>

#include <tf/tf.h>
#include <tf/transform_datatypes.h>
#include <tf2_geometry_msgs/tf2_geometry_msgs.h>
#include <tf2_ros/buffer.h>
#include <tf2_ros/transform_listener.h>
#include <tf2_ros/transform_broadcaster.h>
#include <tf2_eigen/tf2_eigen.h>
#include <XmlRpcException.h>

#include "INS570D/INS570D_Define.h"
#include "INS_STATUS.h"

#include <sys/time.h>
#include <GeographicLib/TransverseMercator.hpp> 



class insDriverPublisher
{
public:
    insDriverPublisher(ros::NodeHandle &nh);
    void longtitude_initialize(INS_STATUS &ins_);
    void ref_initialize(INS_STATUS &ins_);
    void pubGPSFIX(INS_STATUS &ins_);
    void pubGPSFIX(RTK_STATUS &gps_);
    void pubINS(INS_STATUS &ins_);
    void save_ins(INS_STATUS &ins_);
    void pubPOSE(INS_STATUS &ins_);
    void pubIMU(INS_STATUS &ins_);
    void pubVEL(INS_STATUS &ins_);
    void publish(INS_STATUS &ins_);
    void publish(INS_STATUS &ins_,INS_STATUS &imu_ ,RTK_STATUS &rtk_);
    void gps_to_utm(double longtitude, double latitude);

private:
    ros::Publisher rtk_pub_,gps_pub_,ins_pub_,pose_pub_,imu_pub_,vel_pub_;
    
    struct UTM_XY{
        double x;
        double y;
        double gamma;
        double kk;
    };

    UTM_XY utm_xy;

    // double longtitude_0 = 120.0;//UTM区域内，中央子午线.默认设定在上海。
    double longtitude_0 = -122.0;//UTM区域内，中央子午线,设定在sunnyvale，实际会以上电初始化时刻的经度值替代。

    bool longtitude_initialized = false;

    geometry_msgs::Pose ref_pos;
    bool ref_initialized = false;

    double a = 6378137;
    double f = 1 / 298.257223563;
    double k = 0.9996;
    GeographicLib::TransverseMercator utm = GeographicLib::TransverseMercator(a,f,k);

    
};

#endif // INSDRIVERPUBLISHER_H
