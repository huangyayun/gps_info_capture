#include <sys/types.h>
#include <sstream>
#include <unistd.h>

#include <ros/ros.h>
#include <std_msgs/String.h>
#include <serial/serial.h>
#include <XmlRpcException.h>

#include <sensor_msgs/Imu.h>
#include <sensor_msgs/NavSatFix.h>
#include <sensor_msgs/NavSatStatus.h>

#include <iomanip>//保留有效小数
#include <getopt.h>

#include "INS570D/INS570D_Driver.h"
#include "insdriverpublisher.h"

bool openSerialPort(serial::Serial &serial_,
                    const std::string serial_port_,
                    const uint32_t baud_rate_,
                    serial::Timeout to = serial::Timeout::simpleTimeout(10)){

    serial_.setPort(serial_port_);
    //设置串口通信的波特率
    serial_.setBaudrate(230400);
    //串口设置timeout
    serial_.setTimeout(to);

    try
    {
        //打开串口
        serial_.open();
    }
    catch(serial::IOException& e)
    {
        ROS_ERROR_STREAM("Unable to open port.");
        return -1;
    }

    //判断串口是否打开成功
    if(serial_.isOpen())
    {
        ROS_INFO_STREAM(serial_port_<<" is opened.");
    }
    else
    {
        return -1;
    }
}

int main(int argc, char **argv)
{
    ros::init(argc, argv, "gps_ins");
    ros::NodeHandle nh;

    //创建一个serial类
    serial::Serial sp;
    //创建timeout
    serial::Timeout to = serial::Timeout::simpleTimeout(10);

    // std::cout << "inter_byte_timeout: " << to.inter_byte_timeout << std::endl;
    // std::cout << "read_timeout_constant: " << to.read_timeout_constant << std::endl;
    // std::cout << "read_timeout_multiplier: " << to.read_timeout_multiplier << std::endl;

    //设置要打开的串口名称
    std::string serial_port = "/dev/ttyUSB0";
    int baud_rate = 230400;
    bool use_GNSS_time = false;
    double gravity_constant = 9.81;
    std::string insDriver_type = "ins570D";

	static struct option long_options[] = {
		/* These options set a flag. */
		{"port", required_argument, 0, 'p'},
		{"baudrate", required_argument, 0, 'b'},
		{0, 0, 0, 0}};

    /* getopt_long stores the option index here. */
    int c;
    while (1)
    {
        int option_index = 0;
        c = getopt_long(argc, argv, "p:b:",
                        long_options, &option_index);

        /* Detect the end of the options. */
        if (c == -1)
        {
            break;
        }

        switch (c)
        {
        case 0:
            break;

        case 'p':
            serial_port = strdup(optarg);
            break;

        case 'b':
            baud_rate = atoi(optarg);
            break;

        default:
            abort();
        }
    }        

    ROS_INFO("Serial Port: %s",serial_port.c_str());
    ROS_INFO("baud_rate: %d",baud_rate);

    //============================打开串口==================================
    openSerialPort(sp,serial_port,baud_rate,to);

    //============================解析和发布================================
    std::shared_ptr<insDriverPublisher> publisher_=std::make_shared<insDriverPublisher>(nh);
    std::shared_ptr<BufferParaseInterface> driver_= std::make_shared<INS570D_Driver>(63);

    ros::Rate loop_rate(1000); // 循环频率，数值越大，频率越快
    while(ros::ok())
    {
        /// TODO : buffer缓冲队列
        std::vector<uint8_t> buf;
        // std::cout<<"Start Reading"<<std::endl;
        sp.read(buf, 63);

        ros::Time now = ros::Time::now();        
        ROS_INFO("time: %d ms, buf.size = %d", now.sec * 1000 + now.nsec / 1000000, buf.size());
        if (buf.size() == 63)
        {
            ROS_INFO("%02X, %02X, %02X, %02X, %02X, %02X", buf[0], buf[1], buf[2], buf[60], buf[61], buf[62]);
            bool checksum = driver_->setBuffer(buf);
            if (checksum)
            {
                driver_->parse();                
            }
            else
            {
                std::cout<<"check sum error"<<std::endl;
            }
            publisher_->publish(driver_->getInsData());
        }
        loop_rate.sleep();
    }
    //关闭串口
    sp.close();
    ros::spin();
    return 0;
}
