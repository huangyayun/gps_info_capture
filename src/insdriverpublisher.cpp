#include "insdriverpublisher.h"

#include <dirent.h>
#include <fstream>
#include <string>
#include <boost/filesystem.hpp>

extern std::string main_path;

insDriverPublisher::insDriverPublisher(ros::NodeHandle &nh)
{
    // 发布器：将坐标和速度信息发布，用来记录waypoint ====> [暂时策略]
    // rtk_pub_=nh.advertise<sensor_msgs::NavSatFix>("gps/fix",50);
    gps_pub_=nh.advertise<sensor_msgs::NavSatFix>("gps",50);
    ins_pub_=nh.advertise<nav_msgs::Odometry>("gps/ins",200);
    pose_pub_=nh.advertise<geometry_msgs::PoseStamped>("pose",200);
    imu_pub_=nh.advertise<sensor_msgs::Imu>("imu",200);
    vel_pub_=nh.advertise<geometry_msgs::TwistStamped>("vel",200);
}

// 发布gps/Fix  (RTK Fix)
void insDriverPublisher::pubGPSFIX(INS_STATUS &ins_)
{
//    if(ins_.fix_type!=NARROW_INT)
//        return;
    sensor_msgs::NavSatFix gpsFix_;
    gpsFix_.header.stamp.fromSec(ins_.unix_timestamp);
    gpsFix_.header.frame_id="/gps";

    // if(ins_.fix_type!=NARROW_INT)
    //     gpsFix_.status.status=sensor_msgs::NavSatFix::_status_type::STATUS_NO_FIX;
    // else if(ins_.fix_type==NARROW_INT)
    //     gpsFix_.status.status=sensor_msgs::NavSatFix::_status_type::STATUS_FIX;

    gpsFix_.status.status = ins_.fix_type;

    gpsFix_.status.service=sensor_msgs::NavSatFix::_status_type::SERVICE_GLONASS;

    gpsFix_.latitude=ins_.latitude;
    gpsFix_.longitude=ins_.longitude;
    gpsFix_.altitude=ins_.altitude;

    gpsFix_.position_covariance_type=sensor_msgs::NavSatFix::COVARIANCE_TYPE_KNOWN;

    gpsFix_.position_covariance[0]=pow(ins_.lat_std,2);
    gpsFix_.position_covariance[4]=pow(ins_.lon_std,2);
    gpsFix_.position_covariance[8]=pow(ins_.alti_std,2);

    gps_pub_.publish(gpsFix_);
    // if(ins_.fix_type==NARROW_INT)
        // rtk_pub_.publish(gpsFix_);

    // //
    // uint64_t timestamp = gpsFix_.header.stamp.sec * uint64_t(1000000) + gpsFix_.header.stamp.nsec / 1000;
    // std::string sub_path = main_path + "/gps/";
   


    // std::string file_name = sub_path + "gps_imu_" + std::to_string(timestamp) + ".txt";
    // std::fstream out(file_name, std::ios::out);
    // out << std::fixed << std::setprecision(8);
    // out << gpsFix_.latitude << std::endl;
    // out << gpsFix_.longitude << std::endl;
    // out << gpsFix_.altitude << std::endl;

    // out << gpsFix_.position_covariance[0] << std::endl;
    // out << gpsFix_.position_covariance[4] << std::endl;
    // out << gpsFix_.position_covariance[8] << std::endl;

    // geometry_msgs::Quaternion q=tf::createQuaternionMsgFromRollPitchYaw(ins_.roll,ins_.pitch,ins_.yaw);

    // out << q.w << std::endl;
    // out << q.x << std::endl;
    // out << q.y << std::endl;
    // out << q.z << std::endl;

    // out << pow(ins_.roll_std,2) << std::endl;
    // out << pow(ins_.pitch_std,2) << std::endl;
    // out << pow(ins_.yaw_std,2) << std::endl;

    // out << int(ins_.fix_type) << std::endl;

    // out.close();    
    //         
}


void insDriverPublisher::pubINS(INS_STATUS &ins_)
{
    nav_msgs::Odometry odom_ins_;
    odom_ins_.header.stamp.fromSec(ins_.unix_timestamp);
    odom_ins_.header.frame_id="/map";           //
    odom_ins_.child_frame_id="/gps";

    odom_ins_.pose.pose.position.x=ins_.longitude;
    odom_ins_.pose.pose.position.y=ins_.latitude;
    odom_ins_.pose.pose.position.z=ins_.altitude;

    geometry_msgs::Quaternion q=tf::createQuaternionMsgFromRollPitchYaw(ins_.roll,ins_.pitch,ins_.yaw);
    odom_ins_.pose.pose.orientation=q;

    // printf("%f %f %f %f\n",q.w, q.x, q.y, q.z);

    odom_ins_.pose.covariance[0]=pow(ins_.lon_std,2);
    odom_ins_.pose.covariance[7]=pow(ins_.lat_std,2);
    odom_ins_.pose.covariance[14]=pow(ins_.alti_std,2);
    odom_ins_.pose.covariance[21]=pow(ins_.roll_std,2);
    odom_ins_.pose.covariance[28]=pow(ins_.pitch_std,2);
    odom_ins_.pose.covariance[35]=pow(ins_.yaw_std,2);

    ins_pub_.publish(odom_ins_);   
}

void insDriverPublisher::save_ins(INS_STATUS &ins_)
{
    //保存gps_pos信息
    time_t rawtime = ins_.unix_timestamp; 
    struct tm *timeinfo = localtime(&rawtime);
    char str_time[100];
    sprintf(str_time, "%04d%02d%02d%02d%02d", timeinfo->tm_year+1900, timeinfo->tm_mon+1, timeinfo->tm_mday,timeinfo->tm_hour,timeinfo->tm_min);
    std::string sub_folder(str_time);

    std::string path = main_path + "/" + sub_folder + "/" + "ins" + "/" ;
    
    boost::filesystem::create_directories(path);

    std::string file_name = path + std::to_string(long(ins_.unix_timestamp*1000)) + "_ins.txt";
    std::fstream out(file_name, std::ios::out);
    out << std::fixed << std::setprecision(8);


    out << utm_xy.x << std::endl;
    out << utm_xy.y << std::endl;
    out << ins_.altitude << std::endl;
    out << ins_.lon_std << std::endl;
    out << ins_.lat_std << std::endl;
    out << ins_.alti_std << std::endl;
    out << utm_xy.gamma << std::endl;
    out << utm_xy.kk << std::endl;

    // rpy
    out << ins_.roll << std::endl;
    out << ins_.pitch << std::endl;
    out << ins_.yaw << std::endl;
    out << ins_.roll_std << std::endl;
    out << ins_.pitch_std << std::endl;
    out << ins_.yaw_std << std::endl;

    // velocity ned
    out << ins_.vel_n << std::endl;
    out << ins_.vel_e << std::endl;
    out << ins_.vel_d << std::endl;
    out << ins_.vn_std << std::endl;
    out << ins_.ve_std << std::endl;
    out << ins_.vd_std << std::endl;
    
    // imu output
    out << ins_.acc_x << std::endl;
    out << ins_.acc_y << std::endl;
    out << ins_.acc_z << std::endl;
    out << ins_.gyro_x << std::endl;
    out << ins_.gyro_y << std::endl;
    out << ins_.gyro_z << std::endl;

    // rtk status
    out << int(ins_.fix_type) << std::endl;

    out.close();    

}

void insDriverPublisher::pubPOSE(INS_STATUS &ins_)
{
    
    //发布gps_pose信息
    geometry_msgs::PoseStamped pose;
    pose.header.stamp.fromSec(ins_.unix_timestamp);
    pose.header.frame_id = "/reference_frame_link";
    pose.pose.position.x = utm_xy.x;
    pose.pose.position.y = utm_xy.y;
    pose.pose.position.z = ins_.altitude;
    pose.pose.orientation = tf::createQuaternionMsgFromRollPitchYaw(ins_.roll,ins_.pitch,ins_.yaw);
    pose_pub_.publish(pose);

}

void insDriverPublisher::pubIMU(INS_STATUS &ins_)
{
    sensor_msgs::Imu imu_msg;
    imu_msg.header.stamp.fromSec(ins_.unix_timestamp);
    imu_msg.header.frame_id="/imu_link";

    geometry_msgs::Quaternion q=tf::createQuaternionMsgFromRollPitchYaw(ins_.roll,ins_.pitch,ins_.yaw);
    imu_msg.orientation = q;
    // imu_msg.orientation.w=ins_.qw;
    // imu_msg.orientation.x=ins_.qx;
    // imu_msg.orientation.y=ins_.qy;
    // imu_msg.orientation.z=ins_.qz;

    imu_msg.linear_acceleration.x=ins_.acc_x;
    imu_msg.linear_acceleration.y=ins_.acc_y;
    imu_msg.linear_acceleration.z=ins_.acc_z;

    imu_msg.angular_velocity.x=ins_.gyro_x;
    imu_msg.angular_velocity.y=ins_.gyro_y;
    imu_msg.angular_velocity.z=ins_.gyro_z;

    // 待标定
    imu_msg.linear_acceleration_covariance[0]=pow(0.025,2);
    imu_msg.linear_acceleration_covariance[4]=pow(0.025,2);
    imu_msg.linear_acceleration_covariance[8]=pow(0.025,2);

    imu_msg.angular_velocity_covariance[0]=pow(0.01,2);
    imu_msg.angular_velocity_covariance[4]=pow(0.01,2);
    imu_msg.angular_velocity_covariance[8]=pow(0.01,2);

    imu_msg.orientation_covariance[0]=pow(ins_.roll_std,2);
    imu_msg.orientation_covariance[4]=pow(ins_.pitch_std,2);
    imu_msg.orientation_covariance[8]=pow(ins_.yaw_std,2);

    imu_pub_.publish(imu_msg);
}

void insDriverPublisher::pubVEL(INS_STATUS &ins_)
{
    geometry_msgs::TwistStamped vel_msg;
    vel_msg.header.stamp.fromSec(ins_.unix_timestamp);
    vel_msg.header.frame_id="/imu_link";

    vel_msg.twist.linear.x=ins_.vel_n;
    vel_msg.twist.linear.y=ins_.vel_e;
    vel_msg.twist.linear.z=ins_.vel_d;

    vel_msg.twist.angular.x=ins_.gyro_x;
    vel_msg.twist.angular.y=ins_.gyro_y;
    vel_msg.twist.angular.z=ins_.gyro_z;

    vel_pub_.publish(vel_msg);
}

void insDriverPublisher::gps_to_utm(double lon, double lat)
{
    utm.Forward(longtitude_0,lat,lon,utm_xy.x,utm_xy.y,utm_xy.gamma,utm_xy.kk);//调用TransverseMercator的forward函数，输入

}

void insDriverPublisher::longtitude_initialize(INS_STATUS &ins_)
{
    ROS_INFO("ins status: %d",ins_.fix_type);
    if (!longtitude_initialized && ins_.fix_type==NARROW_INT)
    {
        longtitude_0 = ins_.longitude;
        longtitude_initialized = true;
    }
    return;
}



void insDriverPublisher::publish(INS_STATUS &ins_)
{
    longtitude_initialize(ins_);//初始化参考longtitude,l0
    if (longtitude_initialized == true)
    {
        
        gps_to_utm(ins_.longitude,ins_.latitude);
        
        pubPOSE(ins_);
        
        pubIMU(ins_);
       
        pubVEL(ins_);

        pubINS(ins_);
        

    }

    // ROS_INFO("initialized: %d, long_0: %f, long: %f, lat: %f, x: %f, y: %f", longtitude_initialized, longtitude_0, ins_.longitude, ins_.latitude, utm_xy.x, utm_xy.y);
    return;
}


