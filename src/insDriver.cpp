#include <sys/types.h>
#include <sstream>
#include <unistd.h>

#include <ros/ros.h>
#include <std_msgs/String.h>
#include <serial/serial.h>
#include <XmlRpcException.h>

#include <sensor_msgs/Imu.h>
#include <sensor_msgs/NavSatFix.h>
#include <sensor_msgs/NavSatStatus.h>

#include <iomanip>//保留有效小数
#include <getopt.h>

#include "INS570D/INS570D_Driver.h"
#include "insdriverpublisher.h"

#define PURE_SERIAL 1

#if PURE_SERIAL
#include <errno.h>
#include <fcntl.h> 
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <termios.h>
#include <unistd.h>
#include <iostream>
#endif

bool debug_b = false;

std::string main_path = "/tmp/record";

int set_interface_attribs(int fd, int speed)
{
    struct termios tty;

    if (tcgetattr(fd, &tty) < 0) {
        printf("Error from tcgetattr: %s\n", strerror(errno));
        return -1;
    }

    cfsetospeed(&tty, (speed_t)speed);
    cfsetispeed(&tty, (speed_t)speed);

    tty.c_cflag |= (CLOCAL | CREAD);    /* ignore modem controls */
    tty.c_cflag &= ~CSIZE;
    tty.c_cflag |= CS8;         /* 8-bit characters */
    tty.c_cflag &= ~PARENB;     /* no parity bit */
    tty.c_cflag &= ~CSTOPB;     /* only need 1 stop bit */
    tty.c_cflag &= ~CRTSCTS;    /* no hardware flowcontrol */

    /* setup for non-canonical mode */
    tty.c_iflag &= ~(IGNBRK | BRKINT | PARMRK | ISTRIP | INLCR | IGNCR | ICRNL | IXON);
    tty.c_lflag &= ~(ECHO | ECHONL | ICANON | ISIG | IEXTEN);
    tty.c_oflag &= ~OPOST;

    /* fetch bytes as they become available */
    tty.c_cc[VMIN] = 1;
    tty.c_cc[VTIME] = 1;

    if (tcsetattr(fd, TCSANOW, &tty) != 0) {
        printf("Error from tcsetattr: %s\n", strerror(errno));
        return -1;
    }
    return 0;
}

void set_mincount(int fd, int mcount)
{
    struct termios tty;

    if (tcgetattr(fd, &tty) < 0) {
        printf("Error tcgetattr: %s\n", strerror(errno));
        return;
    }

    tty.c_cc[VMIN] = mcount ? 1 : 0;
    tty.c_cc[VTIME] = 5;        /* half second timer */

    if (tcsetattr(fd, TCSANOW, &tty) < 0)
        printf("Error tcsetattr: %s\n", strerror(errno));
}

bool openSerialPort(serial::Serial &serial_,
                    const std::string serial_port_,
                    const uint32_t baud_rate_,
                    serial::Timeout to = serial::Timeout::simpleTimeout(10)){

    serial_.setPort(serial_port_);
    //设置串口通信的波特率
    serial_.setBaudrate(230400);
    //串口设置timeout
    serial_.setTimeout(to);

    try
    {
        //打开串口
        serial_.open();
    }
    catch(serial::IOException& e)
    {
        ROS_ERROR_STREAM("Unable to open port.");
        return -1;
    }

    //判断串口是否打开成功
    if(serial_.isOpen())
    {
        ROS_INFO_STREAM(serial_port_<<" is opened.");
    }
    else
    {
        return -1;
    }
}

int findStart(const std::vector<uint8_t> &v)
{
    int p = -1;
    for (int i = 0; i < v.size() - 2; i++)
    {
        if ((v[i] == 0xBD) && (v[i+1] == 0xDB) && (v[i+2] == 0x0B)) 
        {
            p = i;
            break;
        }
    }

    return p;
}

int main(int argc, char **argv)
{
    ros::init(argc, argv, "gps_ins");
    ros::NodeHandle nh;
    ros::NodeHandle nh_private("~");

    nh_private.param("record_path",main_path, std::string("/tmp/record"));

    //创建一个serial类
    serial::Serial sp;
    //创建timeout
    serial::Timeout to = serial::Timeout::simpleTimeout(10);

    // std::cout << "inter_byte_timeout: " << to.inter_byte_timeout << std::endl;
    // std::cout << "read_timeout_constant: " << to.read_timeout_constant << std::endl;
    // std::cout << "read_timeout_multiplier: " << to.read_timeout_multiplier << std::endl;

    //设置要打开的串口名称
    std::string serial_port = "/dev/ttyUSB0";
    int baud_rate = 230400;
    bool use_GNSS_time = false;
    double gravity_constant = 9.81;
    std::string insDriver_type = "ins570D";

	static struct option long_options[] = {
		/* These options set a flag. */
		{"port", required_argument, 0, 'p'},
		{"baudrate", required_argument, 0, 'b'},
        {"debug", required_argument, 0, 'd'},
        {"folder", required_argument, 0, 'f'},
		{0, 0, 0, 0}};

    /* getopt_long stores the option index here. */
    int c;
    while (1)
    {
        int option_index = 0;
        c = getopt_long(argc, argv, "p:b:d:f:",
                        long_options, &option_index);

        /* Detect the end of the options. */
        if (c == -1)
        {
            break;
        }

        switch (c)
        {
        case 0:
            break;

        case 'p':
            serial_port = strdup(optarg);
            break;

        case 'b':
            baud_rate = atoi(optarg);
            break;

        case 'd':
          debug_b = atoi(optarg);
          break;            

        case 'f':
          main_path = strdup(optarg);
          break;

        default:
            abort();
        }
    }        

    ROS_INFO("Serial Port: %s",serial_port.c_str());
    ROS_INFO("baud_rate: %d",baud_rate);
    ROS_INFO("main_path: %s", main_path.c_str());

    //============================打开串口==================================
#if PURE_SERIAL
    const char *portname = serial_port.c_str();
    int fd;
    int wlen;

    fd = open(portname, O_RDWR | O_NOCTTY | O_SYNC);
    if (fd < 0) {
        printf("Error opening %s: %s\n", portname, strerror(errno));
        return -1;
    }
    set_interface_attribs(fd, B230400);
#else
    openSerialPort(sp,serial_port,baud_rate,to);
#endif

    //============================解析和发布================================
    std::shared_ptr<insDriverPublisher> publisher_=std::make_shared<insDriverPublisher>(nh);
    std::shared_ptr<BufferParaseInterface> driver_= std::make_shared<INS570D_Driver>(63);

    std::vector<uint8_t> buf_all;
    ros::Rate loop_rate(1000); 
    while(ros::ok())
    {
        // buffer缓冲
#if PURE_SERIAL
        unsigned char buffer[80];
        int rdlen = read(fd, buffer, sizeof(buffer) - 1);
        if (rdlen <= 0)
        {
            continue;
        }
        std::vector<uint8_t> buf(buffer, buffer+rdlen);
#else         
        std::vector<uint8_t> buf;
        // std::cout<<"Start Reading"<<std::endl;
       
        sp.read(buf, sp.available());
#endif

        if (debug_b)
        {
            ros::Time now = ros::Time::now();
            ROS_INFO("time: %d ms, buf.size = %d", now.sec * 1000 + now.nsec / 1000000, buf.size());
        }

        buf_all.insert(buf_all.end(), buf.begin(), buf.end());
        int start_ = -1;
        while(buf_all.size() > 2)
        {
            start_ = findStart(buf_all);
            if (start_ == -1)
            {
                // if (buf_all.size() > 2)
                {
                    buf_all[0] = buf_all[buf_all.size()-2];
                    buf_all[1] = buf_all[buf_all.size()-1];
                    buf_all.resize(2);
                }
            }
            else
            {
                if (start_ + 62 >= buf_all.size())
                {
                    // delete till start_
                    // !!! check start_ == 0                    
                    buf_all.erase(buf_all.begin(), buf_all.begin()+start_);
                    break;
                }
                else
                {
                    // parse and publish
                    //
                    // ROS_INFO("%02X, %02X, %02X", buf_all[start_+0], buf_all[start_+1], buf_all[start_+2]);
                    bool checksum = driver_->setBuffer(buf_all, start_, start_+63);
                    if (checksum)
                    {
                        driver_->parse();
                    }
                    else
                    {
                        std::cout << "check sum error" << std::endl;
                    }
                    publisher_->publish(driver_->getInsData());

                    // delete till end
                    // 
                    buf_all.erase(buf_all.begin(), buf_all.begin()+(start_+63));
                }

            }
        }

        loop_rate.sleep();
    }
    //关闭串口
    sp.close();
    ros::spin();
    return 0;
}
